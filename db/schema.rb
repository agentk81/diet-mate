# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170621113300) do

  create_table "ingredient_categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "ingredients", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.integer  "unit"
    t.integer  "energy"
    t.float    "protein",                limit: 24
    t.float    "carbo",                  limit: 24
    t.float    "fat",                    limit: 24
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "ingredient_category_id"
    t.index ["ingredient_category_id"], name: "index_ingredients_on_ingredient_category_id", using: :btree
  end

  create_table "meals", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "amount"
    t.integer  "mealtime_id"
    t.integer  "recipe_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "type"
    t.integer  "ingredient_id"
    t.string   "name"
    t.integer  "energy"
    t.float    "carbo",         limit: 24
    t.float    "protein",       limit: 24
    t.float    "fat",           limit: 24
    t.index ["ingredient_id"], name: "index_meals_on_ingredient_id", using: :btree
    t.index ["mealtime_id"], name: "index_meals_on_mealtime_id", using: :btree
    t.index ["recipe_id"], name: "index_meals_on_recipe_id", using: :btree
  end

  create_table "mealtimes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "title"
    t.datetime "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "recipe_ingredients", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "amount"
    t.integer  "recipe_id"
    t.integer  "ingredient_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["ingredient_id"], name: "index_recipe_ingredients_on_ingredient_id", using: :btree
    t.index ["recipe_id"], name: "index_recipe_ingredients_on_recipe_id", using: :btree
  end

  create_table "recipes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.integer  "portions"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "weight_protocol_entries", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "date"
    t.float    "weight",     limit: 24
    t.integer  "height"
    t.float    "waist",      limit: 24
    t.float    "hip",        limit: 24
    t.float    "neck",       limit: 24
    t.integer  "sex"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_foreign_key "ingredients", "ingredient_categories"
  add_foreign_key "meals", "ingredients"
  add_foreign_key "meals", "mealtimes"
  add_foreign_key "meals", "recipes"
  add_foreign_key "recipe_ingredients", "ingredients"
  add_foreign_key "recipe_ingredients", "recipes"
end
