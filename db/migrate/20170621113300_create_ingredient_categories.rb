class CreateIngredientCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :ingredient_categories do |t|
      t.string :name
      t.text :description

      t.timestamps
    end

    add_reference :ingredients, :ingredient_category, foreign_key: true, index: true
  end
end
