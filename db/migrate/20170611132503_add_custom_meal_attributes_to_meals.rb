class AddCustomMealAttributesToMeals < ActiveRecord::Migration[5.0]
  def change
    add_column :meals, :name, :string
    add_column :meals, :energy, :integer
    add_column :meals, :carbo, :float
    add_column :meals, :protein, :float
    add_column :meals, :fat, :float
  end
end
