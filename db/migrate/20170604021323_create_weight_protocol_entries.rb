class CreateWeightProtocolEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :weight_protocol_entries do |t|
      t.datetime :date
      t.float :weight
      t.integer :height
      t.float :waist
      t.float :hip
      t.float :neck
      t.integer :sex

      t.timestamps
    end
  end
end
