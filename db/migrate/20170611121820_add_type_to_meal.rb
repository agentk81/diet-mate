class AddTypeToMeal < ActiveRecord::Migration[5.0]
  def change
    rename_column :meals, :portions, :amount
    add_column :meals, :type, :string

    Meal.all.each do |meal|
      meal.type = 'RecipeMeal'
      meal.save
    end
  end
end
