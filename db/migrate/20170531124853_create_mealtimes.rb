class CreateMealtimes < ActiveRecord::Migration[5.0]
  def change
    create_table :mealtimes do |t|
      t.string :title
      t.datetime :date
      t.integer :position

      t.timestamps
    end
  end
end
