class RemovePositionFromMealtime < ActiveRecord::Migration[5.0]
  def change
    remove_column :mealtimes, :position, :integer
  end
end
