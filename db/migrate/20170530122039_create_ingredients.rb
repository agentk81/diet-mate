class CreateIngredients < ActiveRecord::Migration[5.0]
  def change
    create_table :ingredients do |t|
      t.string :name
      t.integer :unit
      t.integer :energy
      t.float :protein
      t.float :carbo
      t.float :fat

      t.timestamps
    end
  end
end
