class AddIngredientToMeal < ActiveRecord::Migration[5.0]
  def change
    add_reference :meals, :ingredient, foreign_key: true
  end
end
