Rails.application.routes.draw do
  resources :ingredients, only: %i[index show new create update destroy]
  resources :ingredient_categories, only: %i[index show new create update destroy]
  resources :recipes, only: %i[index show new create update destroy] do
    resources :recipe_ingredients, only: %i[new]
  end
  resources :recipe_ingredients, only: %i[create update destroy]
  resources :mealtimes, only: %i[index show new create update destroy] do
    resources :recipe_meals, only: %i[new]
    resources :ingredient_meals, only: %i[new]
    resources :custom_meals, only: %i[new show]
  end
  resources :recipe_meals, only: %i[create update destroy]
  resources :ingredient_meals, only: %i[create update destroy]
  resources :custom_meals, only: %i[create update destroy]
  resources :protocols, only: %i[index show new]
  resources :weight_protocol_entries, only: %i[create update destroy]

  namespace :api do
    namespace :v1 do
      resources :ingredients, only: %i[index show create update destroy]
      resources :ingredient_categories, only: %i[index show create update destroy]
    end
  end
end
