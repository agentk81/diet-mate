require 'rails_helper'

RSpec.describe Ingredient, type: :model do
  it 'has a valid factory' do
    expect(FactoryGirl.build(:ingredient)).to be_valid
  end
  it 'is invalid without name' do
    expect(FactoryGirl.build(:ingredient, name: nil)).to_not be_valid
  end
  it 'is invalid without unit' do
    expect(FactoryGirl.build(:ingredient, unit: nil)).to_not be_valid
  end
  it 'is invalid without energy' do
    expect(FactoryGirl.build(:ingredient, energy: nil)).to_not be_valid
  end
  it 'is invalid without protein' do
    expect(FactoryGirl.build(:ingredient, protein: nil)).to_not be_valid
  end
  it 'is invalid without carbo' do
    expect(FactoryGirl.build(:ingredient, carbo: nil)).to_not be_valid
  end
  it 'is invalid without fat' do
    expect(FactoryGirl.build(:ingredient, fat: nil)).to_not be_valid
  end
end
