require 'rails_helper'

RSpec.describe RecipeIngredient, type: :model do
  it 'has a valid factory' do
    expect(FactoryGirl.build(:recipe_ingredient)).to be_valid
  end
end
