require 'rails_helper'

RSpec.describe WeightProtocolEntry, type: :model do
  it 'has a valid factory' do
    expect(FactoryGirl.build(:weight_protocol_entry)).to be_valid
  end
  it 'is invalid without date' do
    expect(FactoryGirl.build(:weight_protocol_entry, date: nil)).to_not be_valid
  end
  it 'is invalid without weight' do
    expect(FactoryGirl.build(:weight_protocol_entry, weight: nil)).to_not be_valid
  end
  it 'is invalid without height' do
    expect(FactoryGirl.build(:weight_protocol_entry, height: nil)).to_not be_valid
  end
  it 'is invalid without sex' do
    expect(FactoryGirl.build(:weight_protocol_entry, sex: nil)).to_not be_valid
  end
  context 'male' do
    it 'is valid without waist and neck' do
      expect(FactoryGirl.build(:weight_protocol_entry, sex: 'male', neck: nil, waist: nil)).to be_valid
    end
    it 'is invalid without waist' do
      expect(FactoryGirl.build(:weight_protocol_entry, sex: 'male', neck: 40, waist: nil)).to_not be_valid
    end
    it 'is invalid without neck' do
      expect(FactoryGirl.build(:weight_protocol_entry, sex: 'male', neck: nil, waist: 84)).to_not be_valid
    end
  end
  context 'female' do
    it 'is valid without waist, hip and neck' do
      expect(FactoryGirl.build(:weight_protocol_entry, sex: 'female', neck: nil, hip: nil, waist: nil)).to be_valid
    end
    it 'is invalid without waist' do
      expect(FactoryGirl.build(:weight_protocol_entry, sex: 'female', neck: 40, hip: 90, waist: nil)).to_not be_valid
    end
    it 'is invalid without hip' do
      expect(FactoryGirl.build(:weight_protocol_entry, sex: 'female', neck: 40, hip: nil, waist: 60)).to_not be_valid
    end
    it 'is invalid without neck' do
      expect(FactoryGirl.build(:weight_protocol_entry, sex: 'female', neck: nil, hip: 90, waist: 60)).to_not be_valid
    end
  end
end
