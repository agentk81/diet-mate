require 'rails_helper'

RSpec.describe Recipe, type: :model do
  it 'has a valid factory' do
    expect(FactoryGirl.build(:recipe)).to be_valid
  end
end
