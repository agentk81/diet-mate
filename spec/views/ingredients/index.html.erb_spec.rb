# require 'rails_helper'
#
# RSpec.describe "ingredients/index", type: :view do
#   before(:each) do
#     assign(:ingredients, [
#       Ingredient.create!(
#         :name => "Name",
#         :unit => 2,
#         :energy => 3,
#         :protein => 4.5,
#         :carbo => 5.5,
#         :fat => 6.5
#       ),
#       Ingredient.create!(
#         :name => "Name",
#         :unit => 2,
#         :energy => 3,
#         :protein => 4.5,
#         :carbo => 5.5,
#         :fat => 6.5
#       )
#     ])
#   end
#
#   it "renders a list of ingredients" do
#     render
#     assert_select "tr>td", :text => "Name".to_s, :count => 2
#     assert_select "tr>td", :text => 2.to_s, :count => 2
#     assert_select "tr>td", :text => 3.to_s, :count => 2
#     assert_select "tr>td", :text => 4.5.to_s, :count => 2
#     assert_select "tr>td", :text => 5.5.to_s, :count => 2
#     assert_select "tr>td", :text => 6.5.to_s, :count => 2
#   end
# end
