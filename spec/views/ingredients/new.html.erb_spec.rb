# require 'rails_helper'
#
# RSpec.describe "ingredients/new", type: :view do
#   before(:each) do
#     assign(:ingredient, Ingredient.new(
#       :name => "MyString",
#       :unit => 1,
#       :energy => 1,
#       :protein => 1.5,
#       :carbo => 1.5,
#       :fat => 1.5
#     ))
#   end
#
#   it "renders new ingredient form" do
#     render
#
#     assert_select "form[action=?][method=?]", ingredients_path, "post" do
#
#       assert_select "input#ingredient_name[name=?]", "ingredient[name]"
#
#       assert_select "input#ingredient_unit[name=?]", "ingredient[unit]"
#
#       assert_select "input#ingredient_energy[name=?]", "ingredient[energy]"
#
#       assert_select "input#ingredient_protein[name=?]", "ingredient[protein]"
#
#       assert_select "input#ingredient_carbo[name=?]", "ingredient[carbo]"
#
#       assert_select "input#ingredient_fat[name=?]", "ingredient[fat]"
#     end
#   end
# end
