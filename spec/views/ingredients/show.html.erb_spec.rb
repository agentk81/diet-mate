# require 'rails_helper'
#
# RSpec.describe "ingredients/show", type: :view do
#   before(:each) do
#     @ingredient = assign(:ingredient, Ingredient.create!(
#       :name => "Name",
#       :unit => 2,
#       :energy => 3,
#       :protein => 4.5,
#       :carbo => 5.5,
#       :fat => 6.5
#     ))
#   end
#
#   it "renders attributes in <p>" do
#     render
#     expect(rendered).to match(/Name/)
#     expect(rendered).to match(/2/)
#     expect(rendered).to match(/3/)
#     expect(rendered).to match(/4.5/)
#     expect(rendered).to match(/5.5/)
#     expect(rendered).to match(/6.5/)
#   end
# end
