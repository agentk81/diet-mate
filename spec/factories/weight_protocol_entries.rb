FactoryGirl.define do
  factory :weight_protocol_entry do
    date "2017-06-04 11:13:23"
    weight 1.5
    height 1
    waist 1.5
    hip ""
    neck 1.5
    sex 'male'
  end
end
