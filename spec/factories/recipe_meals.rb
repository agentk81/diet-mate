FactoryGirl.define do
  factory :recipe_meal do
    amount 1
    mealtime
    recipe
  end
end
