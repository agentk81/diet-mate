FactoryGirl.define do
  factory :recipe do
    name "MyString"
    portions 1
  end
end
