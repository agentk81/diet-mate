FactoryGirl.define do
  factory :ingredient do
    name "MyString"
    unit 1
    energy 1
    protein 1.5
    carbo 1.5
    fat 1.5
  end
end
