FactoryGirl.define do
  factory :ingredient_meal do
    mealtime nil
    ingredient nil
    amount 1
  end
end
