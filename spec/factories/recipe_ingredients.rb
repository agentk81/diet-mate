FactoryGirl.define do
  factory :recipe_ingredient do
    amount 1
    recipe
    ingredient
  end
end
