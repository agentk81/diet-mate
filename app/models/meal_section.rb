class MealSection
  attr_reader :mealtimes, :total_energy, :total_carbo, :total_fat, :total_protein
  
  def initialize(date)
    @mealtimes = Mealtime.where(date: date.beginning_of_day..date.end_of_day).order(:date)
    calculate_totals
  end

  def calculate_totals
    @total_energy = 0
    @total_carbo = 0
    @total_protein = 0
    @total_fat = 0
    @mealtimes.each do |meal|
      @total_energy += meal.energy
      @total_carbo += meal.carbo
      @total_protein += meal.protein
      @total_fat += meal.fat
    end
  end
end
