class IngredientMeal < Meal
  belongs_to :mealtime
  belongs_to :ingredient

  def energy
    (ingredient.energy / 100.0) * amount
  end

  def carbo
    (ingredient.carbo / 100.0) * amount
  end

  def protein
    (ingredient.protein / 100.0) * amount
  end

  def fat
    (ingredient.fat / 100.0) * amount
  end
end
