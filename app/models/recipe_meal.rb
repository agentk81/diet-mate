class RecipeMeal < Meal
  belongs_to :mealtime
  belongs_to :recipe

  def energy
    recipe.energy * amount
  end

  def carbo
    recipe.carbo * amount
  end

  def protein
    recipe.protein * amount
  end

  def fat
    recipe.fat * amount
  end
end
