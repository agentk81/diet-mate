class Ingredient < ApplicationRecord
  has_many :recipe_ingredients
  belongs_to :ingredient_category

  scope :non_categorized, -> { where(ingredient_category: nil) }
  scope :category, ->(category) { where("ingredient_category_id IS NULL OR ingredient_category_id = ?", category.id) }

  enum unit: %i[g ml]

  validates :name, :unit, :energy, :carbo, :protein, :fat, presence: true

  def self.by_category
    result = {}
    IngredientCategory.find_each do |category|
      items = []
      category.ingredients.each do |ingredient|
        items << [ingredient.name, ingredient.id]
      end
      result["#{category.name}"] = items
    end
    result
  end
end
