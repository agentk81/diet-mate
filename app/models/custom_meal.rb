class CustomMeal < Meal
  belongs_to :mealtime

  validates :name, :energy, :carbo, :protein, :fat, presence: true
end
