class Recipe < ApplicationRecord
  has_many :recipe_ingredients, dependent: :destroy
  has_many :mealtimes

  validates :name, :portions, presence: true

  def energy
    sum = 0
    recipe_ingredients.each do |ingredient|
      sum += (ingredient.amount / 100.0) * ingredient.ingredient.energy
    end
    sum / portions
  end

  def carbo
    sum = 0
    recipe_ingredients.each do |ingredient|
      sum += (ingredient.amount / 100.0) * ingredient.ingredient.carbo
    end
    sum / portions
  end

  def protein
    sum = 0
    recipe_ingredients.each do |ingredient|
      sum += (ingredient.amount / 100.0) * ingredient.ingredient.protein
    end
    sum / portions
  end

  def fat
    sum = 0
    recipe_ingredients.each do |ingredient|
      sum += (ingredient.amount / 100.0) * ingredient.ingredient.fat
    end
    sum / portions
  end
end
