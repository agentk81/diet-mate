class Meal < ApplicationRecord
  belongs_to :mealtime

  def recipe_meal?
    instance_of? RecipeMeal
  end

  def ingredient_meal?
    instance_of? IngredientMeal
  end

  def custom_meal?
    instance_of? CustomMeal
  end
end
