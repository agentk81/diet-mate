class WeightProtocolEntry < ApplicationRecord
  enum sex: %i[male female]

  validates :date, :weight, :height, :sex, presence: true
  validate :fat_values_present?
  def bmi
    weight / ((height / 100.0)**2)
  end

  def navy_fat
    case sex
    when 'male'
      return '--' unless waist && neck
      86.010 * Math.log10(waist - neck) - 70.041 * Math.log10(height) + 30.30
    when 'female'
      return '--' unless waist && hip && neck
      163.205 * Math.log10(waist + hip - neck) - 97.684 * Math.log10(height) - 104.912
    end
  end

  def fat_values_present?
    return unless waist || hip || neck
    case sex
    when 'male'
      errors.add(:waist, "Waist can't be blank") unless waist
      errors.add(:neck, "Neck can't be blank") unless neck
    when 'female'
      errors.add(:waist, "Waist can't be blank") unless waist
      errors.add(:hip, "Hip can't be blank") unless hip
      errors.add(:neck, "Neck can't be blank") unless neck
    end
  end
end
