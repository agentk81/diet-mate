class Mealtime < ApplicationRecord
  has_many :meals
  has_many :ingredient_meals

  def energy
    sum = 0
    meals.each do |meal|
      sum += meal.energy
    end
    sum
  end

  def carbo
    sum = 0
    meals.each do |meal|
      sum += meal.carbo
    end
    sum
  end

  def protein
    sum = 0
    meals.each do |meal|
      sum += meal.protein
    end
    sum
  end

  def fat
    sum = 0
    meals.each do |meal|
      sum += meal.fat
    end
    sum
  end
end
