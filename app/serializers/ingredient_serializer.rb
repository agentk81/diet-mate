class IngredientSerializer < ActiveModel::Serializer
  attributes :id, :name, :unit, :energy, :protein, :carbo, :fat

  belongs_to :ingredient_category
end
