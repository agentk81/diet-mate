class MealtimesController < ApplicationController
  before_action :set_mealtime, only: %i[show update destroy]

  # GET /mealtimes
  def index
    dates = Mealtime.all.select("DATE(date) AS dist_date").distinct
    @sections = []
    dates.each do |date|
      @sections << MealSection.new(date["dist_date"])
    end

    puts @sections.to_json
    @mealtimes = Mealtime.all.order(:date)
    calculate_totals
  end

  # GET /mealtimes/1
  def show; end

  # GET /mealtimes/new
  def new
    @mealtime = Mealtime.new
  end

  # POST /mealtimes
  def create
    @mealtime = Mealtime.new(mealtime_params)

    if @mealtime.save
      redirect_to @mealtime, notice: 'Mealtime was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /mealtimes/1
  def update
    if @mealtime.update(mealtime_params)
      redirect_to @mealtime, notice: 'Mealtime was successfully updated.'
    else
      render :show
    end
  end

  # DELETE /mealtimes/1
  def destroy
    @mealtime.destroy
    redirect_to mealtimes_url, notice: 'Mealtime was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_mealtime
    @mealtime = Mealtime.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def mealtime_params
    params.require(:mealtime).permit(:title, :date)
  end

  def calculate_totals
    @total_energy = 0
    @total_carbo = 0
    @total_protein = 0
    @total_fat = 0
    @mealtimes.each do |meal|
      @total_energy += meal.energy
      @total_carbo += meal.carbo
      @total_protein += meal.protein
      @total_fat += meal.fat
    end
  end
end
