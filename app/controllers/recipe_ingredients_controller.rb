class RecipeIngredientsController < ApplicationController
  before_action :set_recipe, only: %i[new]
  before_action :set_recipe_ingredient, only: %i[update destroy]

  # GET /recipe_ingredients/new
  def new
    @recipe_ingredient = RecipeIngredient.new(recipe: @recipe)
  end

  # POST /recipe_ingredients
  def create
    @recipe_ingredient = RecipeIngredient.new(recipe_ingredient_params)

    if @recipe_ingredient.save
      redirect_to @recipe_ingredient.recipe, notice: 'RecipeIngredient was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /recipe_ingredients/1
  def update
    if @recipe_ingredient.update(recipe_ingredient_params)
      redirect_to @recipe_ingredient.recipe, notice: 'RecipeIngredient was successfully updated.'
    else
      render :show
    end
  end

  # DELETE /recipe_ingredients/1
  def destroy
    recipe = @recipe_ingredient.recipe
    @recipe_ingredient.destroy
    redirect_to recipe, notice: 'RecipeIngredient was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_recipe
    @recipe = Recipe.find(params[:recipe_id])
  end

  def set_recipe_ingredient
    @recipe_ingredient = RecipeIngredient.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def recipe_ingredient_params
    params.require(:recipe_ingredient).permit(:amount, :ingredient_id, :recipe_id)
  end
end
