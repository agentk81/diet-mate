class IngredientCategoriesController < ApplicationController
  before_action :set_category, only: %i[show update destroy]

  # GET /categories
  def index
    @categories = IngredientCategory.all
  end

  # GET /categories/1
  def show; end

  # GET /categories/new
  def new
    @category = IngredientCategory.new
  end

  # POST /categories
  def create
    @category = IngredientCategory.new(category_params)

    if @category.save
      redirect_to ingredient_categories_path, notice: 'IngredientCategory was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /categories/1
  # PATCH/PUT /categories/1.json
  def update
    if @category.update(category_params)
      redirect_to ingredient_categories_path, notice: 'IngredientCategory was successfully created.'
    else
      render :show
    end
  end

  # DELETE /categories/1
  def destroy
    @category.destroy
    redirect_to ingredient_categories_url, notice: 'IngredientCategory was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_category
    @category = IngredientCategory.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def category_params
    params.require(:ingredient_category).permit(:name, :desc, ingredient_ids: [])
  end
end
