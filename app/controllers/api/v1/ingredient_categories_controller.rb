class Api::V1::IngredientCategoriesController < ApplicationController
  before_action :set_ingredient_category, only: %i[show update destroy]

  # GET /api/v1/ingredient_categories
  def index
    @ingredient_categories = IngredientCategory.all

    render json: @ingredient_categories
  end

  # GET /api/v1/ingredient_categories/1
  def show
    render json: @ingredient_category
  end

  # POST /api/v1/ingredient_categories
  def create
    @ingredient_category = IngredientCategory.new(ingredient_category_params)

    if @ingredient_category.save
      render json: @ingredient_category, location: api_v1_ingredient_category_path(@ingredient_category)
    else
      render json: { errors: @ingredient_category.errors }, status: :bad_request
    end
  end

  # PATCH/PUT /api/v1/ingredient_categories/1
  def update
    if @ingredient_category.update(ingredient_category_params)
      render json: @ingredient_category, location: api_v1_ingredient_category_path(@ingredient_category)
    else
      render json: { errors: @ingredient_category.errors }, status: :bad_request
    end
  end

  # DELETE /api/v1/ingredient_categories/1
  def destroy
    @ingredient_category.destroy
    head :no_content
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_ingredient_category
    @ingredient_category = IngredientCategory.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def ingredient_category_params
    params.require(:ingredient_category).permit(:name, :unit, :energy, :protein, :carbo, :fat, :ingredient_category_category_id)
  end
end
