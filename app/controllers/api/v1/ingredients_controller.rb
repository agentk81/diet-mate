class Api::V1::IngredientsController < ApplicationController
  before_action :set_ingredient, only: %i[show update destroy]

  # GET /api/v1/ingredients
  def index
    @ingredients = Ingredient.all

    render json: @ingredients
  end

  # GET /api/v1/ingredients/1
  def show
    render json: @ingredient
  end

  # POST /api/v1/ingredients
  def create
    @ingredient = Ingredient.new(ingredient_params)

    if @ingredient.save
      render json: @ingredient, location: api_v1_ingredient_path(@ingredient)
    else
      render json: { errors: @ingredient.errors }, status: :bad_request
    end
  end

  # PATCH/PUT /api/v1/ingredients/1
  def update
    if @ingredient.update(ingredient_params)
      render json: @ingredient, location: api_v1_ingredient_path(@ingredient)
    else
      render json: { errors: @ingredient.errors }, status: :bad_request
    end
  end

  # DELETE /api/v1/ingredients/1
  def destroy
    @ingredient.destroy
    head :no_content
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_ingredient
    @ingredient = Ingredient.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def ingredient_params
    params.require(:ingredient).permit(:name, :unit, :energy, :protein, :carbo, :fat, :ingredient_category_id)
  end
end
