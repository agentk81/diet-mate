class IngredientMealsController < ApplicationController
  before_action :set_mealtime, only: %i[new]
  before_action :set_ingredient_meal, only: %i[update destroy]

  # GET /meals/new
  def new
    @meal = IngredientMeal.new(mealtime: @mealtime)
  end

  # POST /meals
  def create
    @meal = IngredientMeal.new(meal_params)

    if @meal.save
      redirect_to @meal.mealtime, notice: 'Meal was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /meals/1
  def update
    if @meal.update(meal_params)
      redirect_to @meal.mealtime, notice: 'Meal was successfully updated.'
    else
      render :show
    end
  end

  # DELETE /meals/1
  def destroy
    mealtime = @meal.mealtime
    @meal.destroy
    redirect_to mealtime, notice: 'Meal was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_mealtime
    @mealtime = Mealtime.find(params[:mealtime_id])
  end

  def set_ingredient_meal
    @meal = IngredientMeal.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def meal_params
    params.require(:ingredient_meal).permit(:amount, :ingredient_id, :mealtime_id)
  end
end
