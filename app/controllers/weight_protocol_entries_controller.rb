class WeightProtocolEntriesController < ApplicationController
  before_action :set_weight_protocol_entry, only: %i[update destroy]

  # POST /weight_protocol_entries
  def create
    @weight_protocol_entry = WeightProtocolEntry.new(weight_protocol_entry_params)

    if @weight_protocol_entry.save
      redirect_to protocols_path, notice: 'Protocol entry was successfully created.'
    else
      render 'protocols/new'
    end
  end

  # PATCH/PUT /weight_protocol_entries/1
  def update
    if @weight_protocol_entry.update(weight_protocol_entry_params)
      redirect_to protocols_path, notice: 'Protocol entry was successfully updated.'
    else
      render 'protocols/show'
    end
  end

  # DELETE /weight_protocol_entries/1
  def destroy
    @weight_protocol_entry.destroy
    redirect_to protocols_path, notice: 'Protocol entry was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_weight_protocol_entry
    @weight_protocol_entry = WeightProtocolEntry.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def weight_protocol_entry_params
    params.require(:weight_protocol_entry).permit(:date, :weight, :height, :waist, :hip, :neck, :sex)
  end
end
