class RecipeMealsController < ApplicationController
  before_action :set_mealtime, only: %i[new]
  before_action :set_meal, only: %i[update destroy]

  # GET /meals/new
  def new
    @meal = RecipeMeal.new(mealtime: @mealtime)
  end

  # POST /meals
  def create
    @meal = RecipeMeal.new(meal_params)

    if @meal.save
      redirect_to @meal.mealtime, notice: 'RecipeMeal was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /meals/1
  def update
    if @meal.update(meal_params)
      redirect_to @meal.mealtime, notice: 'RecipeMeal was successfully updated.'
    else
      render :show
    end
  end

  # DELETE /meals/1
  def destroy
    mealtime = @meal.mealtime
    @meal.destroy
    redirect_to mealtime, notice: 'RecipeMeal was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_mealtime
    @mealtime = Mealtime.find(params[:mealtime_id])
  end

  def set_meal
    @meal = Meal.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def meal_params
    params.require(:recipe_meal).permit(:amount, :recipe_id, :mealtime_id)
  end
end
