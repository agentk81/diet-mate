class ProtocolsController < ApplicationController
  before_action :set_weight_protocol_entry, only: %i[show]

  # GET /weight_protocol_entries
  def index
    @weight_protocol_entries = WeightProtocolEntry.all.order(:date)
  end

  # GET /weight_protocol_entries/1
  def show; end

  # GET /weight_protocol_entries/new
  def new
    @weight_protocol_entry = WeightProtocolEntry.new
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_weight_protocol_entry
    @weight_protocol_entry = WeightProtocolEntry.find(params[:id])
  end
end
